#ifndef CPP_PROJECT_STRUCTURE_LIBFOO_INCLUDE_LIBFOO_FOO_HPP_
#define CPP_PROJECT_STRUCTURE_LIBFOO_INCLUDE_LIBFOO_FOO_HPP_

/**
 * @brief Foo class
 */
class Foo {
public:
  /**
   * @brief Greets the World
   */
  static void getGreeting() noexcept;
  /**
   * @brief Gives five
   * @return the number 5
   */
  static int giveMeFive() noexcept;
};

#endif // CPP_PROJECT_STRUCTURE_LIBFOO_INCLUDE_LIBFOO_FOO_HPP_
