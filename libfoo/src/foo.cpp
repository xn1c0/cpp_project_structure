#include <fmt/color.h>
#include <libfoo/foo.hpp>

void Foo::getGreeting() noexcept {
  fmt::print(fg(fmt::terminal_color::cyan), "Hello fmt {}!\n", FMT_VERSION);
}
int Foo::giveMeFive() noexcept { return 5; }
