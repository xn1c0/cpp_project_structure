cmake_minimum_required(VERSION 3.27)

### Project
project(cpp_project_structure VERSION 1.0 LANGUAGES CXX)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)


### Packages
find_package(fmt REQUIRED)
find_package(doctest REQUIRED)
find_package(Doxygen REQUIRED)


### Subdirectories (the order is important)
add_subdirectory(libfoo)
add_subdirectory(standalone)
