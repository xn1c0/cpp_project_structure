add_executable(standalone main.cpp)
target_link_libraries(standalone PRIVATE fmt::fmt libfoo::libfoo)
target_compile_options(standalone PRIVATE -Wall -Wextra -pedantic -Werror)
target_compile_features(standalone PRIVATE cxx_std_17)
